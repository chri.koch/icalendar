package icalendar

import (
	"fmt"
	"strings"
	"time"
)

type ICalendar struct {
	events []vEvent
}

var eventsCreated int = 0

type vEvent struct {
	start    time.Time
	end      time.Time
	created  time.Time
	id       int
	summary  string
	location string
}

func NewEvent(start, end time.Time, summary string) vEvent {

	eventsCreated++

	e := vEvent{}
	e.start = start
	e.end = end
	e.created = time.Now()
	e.summary = summary
	e.id = eventsCreated

	return e
}

func (e *vEvent) SetLocation(loc string) {
	e.location = loc
}

func (i *ICalendar) AddEvent(e vEvent) {
	i.events = append(i.events, e)
}

func (i *ICalendar) ProdID() string {
	return "-//Christian Koch//Christian Koch//DE"
}

func (e *vEvent) SetCreatedTime(created time.Time) {
	e.created = created
}

func (e *vEvent) DtStamp() string {
	utc := e.created.In(time.UTC)
	return utc.Format("20060102T150405Z")
}

func (e *vEvent) DtStart() string {
	utc := e.start.In(time.UTC)
	return utc.Format("20060102T150405Z")
}

func (e *vEvent) DtEnd() string {
	utc := e.end.In(time.UTC)
	return utc.Format("20060102T150405Z")
}

func (e *vEvent) Uid() string {
	s := fmt.Sprintf("%v-%v@ahacalendar@christiankoch", e.id, e.DtStamp())

	return s
}

func (e *vEvent) Summary() string {
	return strings.Replace(e.summary, "\n", "", -1)
}

func (e *vEvent) Location() string {
	return strings.Replace(e.location, "\n", "", -1)
}

func (i *ICalendar) FullFile() string {
	var s string

	s = "BEGIN:VCALENDAR\n"
	s += "VERSION:2.0\n"
	s += "PRODID:" + i.ProdID() + "\n"

	for _, event := range i.events {
		s += "BEGIN:VEVENT\n"

		s += "SUMMARY:" + event.Summary() + "\n"
		s += "UID:" + event.Uid() + "\n"
		s += "DTSTAMP:" + event.DtStamp() + "\n"
		s += "DTSTART:" + event.DtStart() + "\n"
		s += "DTEND:" + event.DtEnd() + "\n"

		if len(event.Location()) >= 1 {
			s += "LOCATION:" + event.Location() + "\n"
		}

		s += "END:VEVENT\n"
	}

	s += "END:VCALENDAR"

	return s
}
