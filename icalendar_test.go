package icalendar

import (
	"testing"
	"time"
)

func TestEmptyCalendar(t *testing.T) {
	cal := ICalendar{}

	expected := "BEGIN:VCALENDAR\n"
	expected += "VERSION:2.0\n"
	expected += "PRODID:-//Christian Koch//Christian Koch//DE\n"
	expected += "END:VCALENDAR"

	if cal.FullFile() != expected {
		t.Error("Mismatch, empty calendar is " + cal.FullFile())
	}
}

func TestOneEvent(t *testing.T) {

	start, _ := time.ParseInLocation("02.01.2006 15:04", "04.02.2016 20:47", time.Local)
	end, _ := time.ParseInLocation("02.01.2006 15:04", "04.02.2016 20:47", time.Local)

	createdTime := time.Now()

	ev := NewEvent(start, end, "I am the summary")
	ev.SetCreatedTime(createdTime)

	cal := ICalendar{}

	cal.AddEvent(ev)

	expected := "BEGIN:VCALENDAR\n"
	expected += "VERSION:2.0\n"
	expected += "PRODID:-//Christian Koch//Christian Koch//DE\n"
	expected += "BEGIN:VEVENT\n"
	expected += "SUMMARY:I am the summary\n"
	expected += "UID:" + ev.Uid() + "\n"
	expected += "DTSTAMP:" + createdTime.In(time.UTC).Format("20060102T150405Z") + "\n"
	expected += "DTSTART:20160204T194700Z\n"
	expected += "DTEND:20160204T194700Z\n"
	expected += "END:VEVENT\n"
	expected += "END:VCALENDAR"

	if cal.FullFile() != expected {
		t.Error("Mismatch, one event calendar is " + cal.FullFile() + ", but expected is " + expected)
	}
}
